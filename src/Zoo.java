import animals.*;
import employee.Worker;
import food.Food;
import food.Grass;
import food.Meat;
import model.Aviary;
import model.Size;
import model.WrongSizeException;

public class Zoo {

    public static Aviary<Carnivorous> carnivorousAviary = new Aviary(Size.SMALL);
    public static Aviary<Herbivore> herbivoreAviary = new Aviary(Size.SMALL);

    public static boolean compareVoice(Kotik first, Kotik second){

        return first.getVoice().equals(second.getVoice())? true: false;
    }

    public static void fillCarnivorousAviary() throws WrongSizeException {
        Kotik kotik = new Kotik("Котик");

        carnivorousAviary.addAnimal(kotik);
    }
    public static void fillHerbivoreAviary() throws WrongSizeException {
        Duck duck = new Duck("Утка1");

        herbivoreAviary.addAnimal(duck);
    }

    public static Carnivorous getCarnivorous(String name){
        return carnivorousAviary.getAnimal(name);
    }

    public static Herbivore getHerbivore(String name){
        return herbivoreAviary.getAnimal(name);
    }


    public static void main(String[] args) {

        Worker worker = new Worker();

        Grass grass = new Grass();
        Meat meat = new Meat();

        Duck duck = new Duck("Утка1");
        duck.fly();
        duck.run();
        duck.swim();
        worker.feed(duck, grass);
        System.out.println(worker.getVoice(duck));
        System.out.println("Уровень сытости: " + duck.getSatiety());
        System.out.println();

        Fish fish1 = new Fish("Рыба1");
        fish1.swim();
        worker.feed(fish1, meat);
        System.out.println("Уровень сытости: " + fish1.getSatiety());
        System.out.println();

        Elephant elephant = new Elephant("Слон");
        elephant.swim();
        elephant.run();
        System.out.println(worker.getVoice(elephant));
        worker.feed(elephant, grass);
        System.out.println("Уровень сытости: " + elephant.getSatiety());
        System.out.println();

        Lion lion1 = new Lion("Лев1");
        lion1.swim();
        lion1.run();
        System.out.println(worker.getVoice(lion1));
        worker.feed(lion1, meat);
        System.out.println("Уровень сытости: " + lion1.getSatiety());
        System.out.println();

        Rabbit rabbit = new Rabbit("Кролик1");
        rabbit.swim();
        rabbit.run();
        worker.feed(rabbit, grass);
        System.out.println("Уровень сытости: " + rabbit.getSatiety());
        System.out.println();

        Kotik kotik1 = new Kotik("Котик1");
        kotik1.swim();
        kotik1.run();
        worker.feed(kotik1, meat);
        System.out.println("Уровень сытости: " + kotik1.getSatiety());
        System.out.println();


        Kotik vasya = new Kotik("Вася", "Мяу-мау", 4, 5);
        vasya.swim();
        vasya.run();
        worker.feed(vasya, meat);
        System.out.println("Уровень сытости: " + vasya.getSatiety());
        System.out.println(worker.getVoice(vasya));

        Animal[] swimmers = createPound();
        for(Animal animal: swimmers){
                Swim swimmer = (Swim) animal;
                swimmer.swim();
        }

        Kotik barsik = new Kotik("Kotik");
        barsik.setName("Барсик");
        barsik.setVoice("Мяу-мяу");
        barsik.setSatiety(3);
        barsik.setWeigth(4);

        try {
            fillCarnivorousAviary();
            fillHerbivoreAviary();
        } catch (WrongSizeException e) {
            e.printStackTrace();
        }

        for(String action: vasya.liveAnotherDay()){
            System.out.println(action);
        }

        System.out.print("Имя: "+ barsik.getName());
        System.out.println(" | Вес: " + barsik.getWeigth());
        System.out.println("Результат сравнения переменных voice у заданных объектов: "+ compareVoice(vasya, barsik));
        System.out.println("Колличество созданных объектов типа \"Kotik\": " + Kotik.getCount());

    }
    public static Animal[] createPound(){
        Animal[] swimmers = new Animal[3];

        Elephant elephant1 = new Elephant("Cлон1");
        Duck duck = new Duck("Утка2");
        Fish fish = new Fish("Рыба2");
        swimmers[0] = elephant1;
        swimmers[1] = duck;
        swimmers[2] = fish;

        return swimmers;
    }

}

