package animals;

import model.Size;

public class Lion extends Carnivorous implements Voice, Swim, Run {
    public Lion(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println("Лев бежит.");

    }

    @Override
    public void swim() {
        System.out.println("Лев плавает.");

    }

    @Override
    public String getVoice() {
        return "Рррааау!";
    }

    public Size getSize(){
        return Size.LARGE;
    }
    public String getName(){
        return super.name;
    }
}
