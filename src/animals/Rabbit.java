package animals;

import model.Size;

public class Rabbit extends Herbivore implements Swim, Run {
    public Rabbit(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println("Кролик бежит.");

    }

    @Override
    public void swim() {
        System.out.println("Кролик плавает.");
    }

    public Size getSize(){
        return Size.SMALL;
    }
    public String getName(){
        return super.name;
    }
}
