package animals;

import food.Food;
import food.Grass;
import food.Meat;
import food.WrongFoodException;
import model.Size;
import model.WrongSizeException;

public abstract class Carnivorous extends Animal {
    Carnivorous(String name) {
        super(name);
    }
    @Override
    public void eat(Food food) throws WrongFoodException {
            if (food instanceof Grass) {
                throw new WrongFoodException();
            }
            else if(food instanceof Meat) {
                System.out.println("*ест мясо*");
                super.satiety += food.getEnergy();
            }
    }

}
