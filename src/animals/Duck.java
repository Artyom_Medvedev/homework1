package animals;

import model.Size;

public class Duck extends Herbivore implements Voice, Swim, Run, Fly{

    public Duck(String name) {
        super(name);
    }

    @Override
    public void fly() {
        System.out.println("Утка летит.");
    }

    @Override
    public void run() {
        System.out.println("Утка бежит.");
    }

    @Override
    public void swim() {
        System.out.println("Утка плавает.");

    }

    @Override
    public String getVoice() {
        return "Кря-кря!";
    }

    public Size getSize(){
        return Size.SMALL;
    }

    public String getName(){
        return super.name;
    }
}
