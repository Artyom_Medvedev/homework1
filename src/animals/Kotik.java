package animals;

import food.Food;
import food.Meat;
import model.Size;

public class Kotik extends Carnivorous implements Voice, Swim, Run {
    private String voice;
    private int satiety;
    private int weigth;
    private static int count = 0;

    private final int METHODS = 5;

    public Kotik(String name, String voice, int satiety, int weigth){
        super(name);
        this.voice = voice;
        this.satiety = satiety;
        this.weigth = weigth;
        count++;
    }

    public Kotik(String name) {
        super(name);
        count++;
    }

    public String getName() {
        return super.name;
    }
    @Override
    public String getVoice() {
        return voice;
    }
    public int getWeigth(){
        return weigth;
    }
    public static int getCount(){
        return count;
    }
    public Size getSize(){
        return Size.SMALL;
    }

    public void setName(String name){
        super.name = name;
    }
    public void setVoice(String voice){
        this.voice = voice;
    }
    public void setSatiety(int satiety){
        this.satiety = satiety;
    }
    public void setWeigth(int weigth){
        this.weigth = weigth;
    }

    public boolean play(){
        if(satiety>0){
            satiety--;
            System.out.println(name + " играет!");
            return true;
        } else {
            System.out.println("Хочу есть!");
        }
        return false;
    }
    public boolean sleep(){
        if(satiety>0) {
            satiety--;
            System.out.println(name + " спит...");
            return true;
        } else {
            System.out.println("Хочу есть!");
        }
        return false;
    }
    public boolean wash(){
        if(satiety>0) {
            satiety--;
            System.out.println(name + " моется.");
            return true;
        } else {
            System.out.println("Хочу есть!");
        }
        return false;
    }
    public boolean walk(){
        if(satiety>0){
            satiety--;
            System.out.println(name + " гуляет.");
            return true;
        } else {
            System.out.println("Хочу есть!");
        }
        return false;
    }
    public boolean hunt(){
        if(satiety>0){
            satiety--;
            System.out.println(name + " охотится.");
            return true;
        } else {
            System.out.println("Хочу есть!");
        }
        return false;
    }

    public void eat(int satiety){
        this.satiety += satiety;
        System.out.println(name + " ест.");

    }

    private String eat(int satiety, String food){
        this.satiety += satiety;
        return "ел";
    }

    public String eat(){
        return eat(1, "feed");
    }


    @Override
    public void run() {
        System.out.println(this.name +" бежит.");

    }

    @Override
    public void swim() {
        System.out.println(this.name + " плавает.");

    }

    public String[] liveAnotherDay(){
        String[] actions = new String[24];
        for(int i = 0; i<actions.length; i++){
            int action = (int) (Math.random() * METHODS) + 1;
            switch (action) {
                case 1:
                    actions[i] = play()? i + " - играл": i + " - " + eat();
                    break;
                case 2:
                    actions[i] = wash()? i + " - мылся": i + " - " + eat();
                    break;
                case 3:
                    actions[i] = hunt()? i + " - охотился": i + " - " + eat();
                    break;
                case 4:
                    actions[i] = walk()? i + " - гулял": i + " - " + eat();
                    break;
                case 5:
                    actions[i] = sleep()? i + " - спал": i + " - " + eat();
                    break;
            }
        }
        return actions;
    }

}

