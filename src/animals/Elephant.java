package animals;

import model.Size;

public class Elephant extends Herbivore implements Swim, Voice, Run {
    public Elephant(String name) {
        super(name);
    }

    public String getName(){
        return super.name;
    }

    @Override
    public void run() {
        System.out.println("Слон бежит.");

    }

    @Override
    public void swim() {
        System.out.println("Слон плавает.");
    }

    @Override
    public String getVoice() {
        return "Ту-тууу!";
    }

    public Size getSize(){
        return Size.LARGE;
    }
}
