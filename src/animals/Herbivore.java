package animals;

import food.Food;
import food.Grass;
import food.Meat;
import food.WrongFoodException;
import model.Size;

public abstract class Herbivore extends Animal{
    Herbivore(String name) {
        super(name);
    }

    @Override
    public void eat(Food food) throws WrongFoodException {
        if(food instanceof Meat){
            throw new WrongFoodException();
        }
        else if(food instanceof Grass){
            System.out.println("*ест растение*");
            super.satiety += food.getEnergy();
        }
    }

}
