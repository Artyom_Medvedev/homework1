package animals;

import food.Food;
import food.WrongFoodException;
import model.Size;

public abstract class Animal {
    int satiety = 0;
    protected String name;

    public Animal(String name){
        this.name = name;
    }

    public int getSatiety(){
        return satiety;
    }

    public abstract void eat(Food food) throws WrongFoodException;

    public abstract Size getSize();

    public abstract String getName();

}
