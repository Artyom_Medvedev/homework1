package employee;

import animals.Animal;
import animals.Voice;
import food.Food;
import food.WrongFoodException;

public class Worker {

    public void feed(Animal animal, Food food){
        try{
            animal.eat(food);
        }
        catch (WrongFoodException w){
            w.printStackTrace();
        }
    }

    public String getVoice(Animal animal){
        try{
            Voice animalVoice = (Voice) animal;
            return animalVoice.getVoice();
        }
        catch (ClassCastException exception){
            throw new ClassCastException("Данное животное не издаёт звуков");
        }

    }
}
