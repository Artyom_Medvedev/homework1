package model;

import animals.Animal;
import animals.Carnivorous;
import animals.Herbivore;

import java.util.HashMap;
import java.util.Map;

public class Aviary <T extends Animal> {

    Size size;

    private Map<String, T> animalHashMap = new HashMap<>();

    public Aviary(Size size) {
        this.size = size;
    }

    public void addAnimal(T animal) throws WrongSizeException {
        if(size != animal.getSize()){
            throw new WrongSizeException();
        }
            String name = animal.getName();
            animalHashMap.put(name, animal);
    }

    public T getAnimal(String name) {
        T animal = null;
        for(Map.Entry<String, T> pair: animalHashMap.entrySet()) {
            if (pair.getKey().equals(name)) {
                animal = pair.getValue();
            }
        }
        return animal;
    }

    public boolean removeAnimal(String name){
        return animalHashMap.entrySet().removeIf(entry -> animalHashMap.containsKey(name));
    }
}



